/**
 *  @file motor.c
*   @brief [OpenOBC] Motor Driver Implementation File
*/


#include "motor.h"
#include "het.h"

hetSIGNAL_t pwmconf;

uint8 pwmch1[] = { MTR0_FIN, MTR1_FIN, MTR2_FIN };
uint8 pwmch2[] = { MTR0_RIN, MTR1_RIN, MTR2_RIN };

uint8 duties[3][2] = {{0, 0}, {0, 0}, {0, 0}};

uint16 delay = 0;

/**
 * 	@brief Initialize the PWM signals that control the motors (magnetotorquers).
 *
 *	Configure the six PWM signals with a 50% duty cycle and a 50 us period. After that,
 *	put the signals in the Stand-by mode (FIN = 0 and RIN = 0).
 *
 *  @return This function returns nothing.
 */
void mtr_init()
{
	pwmconf.duty = 0;
	pwmconf.period = 25;

	//
	pwmSetSignal(hetRAM1, pwmch1[MOTOR0], pwmconf); // U503
	pwmSetSignal(hetRAM1, pwmch2[MOTOR0], pwmconf);
	pwmStart(hetRAM1, pwmch1[MOTOR0]);
	pwmStart(hetRAM1, pwmch2[MOTOR0]);

	//
	pwmSetSignal(hetRAM1, pwmch1[MOTOR1], pwmconf); // U502
	pwmSetSignal(hetRAM1, pwmch2[MOTOR1], pwmconf);
	pwmStart(hetRAM1, pwmch1[MOTOR1]);
	pwmStart(hetRAM1, pwmch2[MOTOR1]);


	//
	pwmSetSignal(hetRAM1, pwmch1[MOTOR2], pwmconf); // U501
	pwmSetSignal(hetRAM1, pwmch2[MOTOR2], pwmconf);
	pwmStart(hetRAM1, pwmch1[MOTOR2]);
	pwmStart(hetRAM1, pwmch2[MOTOR2]);

	//
	mtr_set_mode(MOTOR0, MTR_MODE_STANDBY, 0x00);
	mtr_set_mode(MOTOR1, MTR_MODE_STANDBY, 0x00);
	mtr_set_mode(MOTOR2, MTR_MODE_STANDBY, 0x00);
}

/**
 * 	@brief Changes the period of the PWM for a specified motor.
 *
 *  This function doesn't change the duty cycle value.
 *
 *	@param p: The new period in microseconds.

 *	@param motor - The H-brigde operating mode:
 	 	 	 	 	  - MTR_MODE_STANDBY
					  - MTR_MODE_FORWARD
					  - MTR_MODE_REVERSE
					  - MTR_MODE_BRAKE
					  - MTR_MODE_FORWARD_PWM_A
					  - MTR_MODE_REVERSE_PWM_A
					  - MTR_MODE_FORWARD_PWM_B
					  - MTR_MODE_REVERSE_PWM_B
 *
 *  @return This function returns nothing.
 */
void mtr_set_period(float64 p, uint8 motor)
{
	if (p > 0)
	{
		hetSIGNAL_t conf1;
		hetSIGNAL_t conf2;

		conf1.duty = duties[motor][0];
		conf2.duty = duties[motor][1];

		conf1.period = p;
		conf2.period = p;

		pwmSetSignal(hetRAM1, pwmch1[motor], conf1);
		pwmSetSignal(hetRAM1, pwmch2[motor], conf2);
	}
}

/**
 * 	@brief Changes the operating mode of a H-bridge for the specified motor and sets the duty cycle.
 *
 *	Changes the operating mode of a H-bridge for the specified motor and sets the duty cycle. This function
 *	stores the current value of the duty cycle for later use together with the mtr_set_period funcition.
 *
 *	@param motor: The ID of the motor:
 	 	 		  	  - MOTOR0
 	 	 		  	  - MOTOR1
 	 	 		  	  - MOTOR2

 *	@param motor - The H-brigde operating mode:
 	 	 	 	 	  - MTR_MODE_STANDBY
					  - MTR_MODE_FORWARD
					  - MTR_MODE_REVERSE
					  - MTR_MODE_BRAKE
					  - MTR_MODE_FORWARD_PWM_A
					  - MTR_MODE_REVERSE_PWM_A
					  - MTR_MODE_FORWARD_PWM_B
					  - MTR_MODE_REVERSE_PWM_B
 *
 *  @return This function returns nothing.
 */
void mtr_set_mode(uint8 motor, uint8 mode, uint8 duty)
{
	// Verify the section '1. Description of Functions' of the datasheet (Page 10/20).
	switch (mode)
	{
		case MTR_MODE_STANDBY: // Mode a.
			pwmSetDuty(hetRAM1, pwmch1[motor], 1);
			for (delay = 0; delay < 1000; delay++) { asm(" nop"); }
			pwmSetDuty(hetRAM1, pwmch1[motor], 0);

			pwmSetDuty(hetRAM1, pwmch2[motor], 1);
			for (delay = 0; delay < 1000; delay++) { asm(" nop"); }
			pwmSetDuty(hetRAM1, pwmch2[motor], 0);
		break;

		case MTR_MODE_FORWARD: // Mode b.
			duties[motor][0] = 100;
			duties[motor][1] = 0;

			pwmSetDuty(hetRAM1, pwmch1[motor], 100);

			pwmSetDuty(hetRAM1, pwmch2[motor], 1);
			for (delay = 0; delay < 1000; delay++) { asm(" nop"); }
			pwmSetDuty(hetRAM1, pwmch2[motor], 0);
		break;

		case MTR_MODE_REVERSE: // Mode c.
			duties[motor][0] = 0;
			duties[motor][1] = 100;

			pwmSetDuty(hetRAM1, pwmch1[motor], 1);
			for (delay = 0; delay < 1000; delay++) { asm(" nop"); }
			pwmSetDuty(hetRAM1, pwmch1[motor], 0);

			pwmSetDuty(hetRAM1, pwmch2[motor], 100);
		break;

		case MTR_MODE_BRAKE: // Mode d.
			pwmSetDuty(hetRAM1, pwmch1[motor], 100);
			pwmStart(hetRAM1, pwmch1[motor]);

			pwmSetDuty(hetRAM1, pwmch2[motor], 100);
			pwmStart(hetRAM1, pwmch2[motor]);
		break;

		case MTR_MODE_FORWARD_PWM_A: // Mode e.
			duties[motor][0] = duty;
			duties[motor][1] = 0;

			pwmSetDuty(hetRAM1, pwmch1[motor], duty);
			pwmSetDuty(hetRAM1, pwmch2[motor], 1);
			for (delay = 0; delay < 1000; delay++) { asm(" nop"); }
			pwmSetDuty(hetRAM1, pwmch2[motor], 0);
		break;

		case MTR_MODE_REVERSE_PWM_A: // Mode f.
			duties[motor][0] = 0;
			duties[motor][1] = duty;

			pwmSetDuty(hetRAM1, pwmch1[motor], 1);
			for (delay = 0; delay < 1000; delay++) { asm(" nop"); }
			pwmSetDuty(hetRAM1, pwmch1[motor], 0);
			pwmSetDuty(hetRAM1, pwmch2[motor], duty);
		break;

		case MTR_MODE_FORWARD_PWM_B: // Mode g.
			duties[motor][0] = 100;
			duties[motor][1] = duty;

			pwmSetDuty(hetRAM1, pwmch1[motor], 100);
			pwmSetDuty(hetRAM1, pwmch2[motor], duty);
		break;

		case MTR_MODE_REVERSE_PWM_B: // Mode h.
			duties[motor][0] = duty;
			duties[motor][1] = 100;

			pwmSetDuty(hetRAM1, pwmch1[motor], duty);
			pwmSetDuty(hetRAM1, pwmch2[motor], 100);
		break;

		default:
		break;
	}
}
