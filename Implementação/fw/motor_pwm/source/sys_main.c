/** @file sys_main.c 
*   @brief Application main file
*   @date 02-Mar-2016
*   @version 04.05.02
*
*   This file contains an empty main function,
*   which can be used for the application.
*/

/* 
* Copyright (C) 2009-2016 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* USER CODE BEGIN (0) */
/* USER CODE END */

/* Include Files */

#include "sys_common.h"

/* USER CODE BEGIN (1) */
#include "het.h"
#include "motor.h"
/* USER CODE END */

/** @fn void main(void)
*   @brief Application main function
*   @note This function is empty by default.
*
*   This function is called after startup.
*   The user can use this function to implement the application.
*/

/* USER CODE BEGIN (2) */
/* USER CODE END */

void main(void)
{
/* USER CODE BEGIN (3) */
	hetInit();
	mtr_init(); //FIN = 0, RIN = 0

	//FIN = PWM, RIN = 0
	mtr_set_mode(MOTOR0, MTR_MODE_FORWARD_PWM_A, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_FORWARD_PWM_A, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_FORWARD_PWM_A, 90);

//	//FIN = 1, RIN = PWM
	mtr_set_mode(MOTOR0, MTR_MODE_FORWARD_PWM_B, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_FORWARD_PWM_B, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_FORWARD_PWM_B, 90);

	//FIN = 0, RIN = PWM
	mtr_set_mode(MOTOR0, MTR_MODE_REVERSE_PWM_A, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_REVERSE_PWM_A, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_REVERSE_PWM_A, 90);

	//FIN = PWM, RIN = 1
	mtr_set_mode(MOTOR0, MTR_MODE_REVERSE_PWM_B, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_REVERSE_PWM_B, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_REVERSE_PWM_B, 90);

	// Changes the period
	mtr_set_period(80, MOTOR0);
	mtr_set_period(80, MOTOR1);
	mtr_set_period(80, MOTOR2);

	//FIN = PWM, RIN = 0
	mtr_set_mode(MOTOR0, MTR_MODE_FORWARD_PWM_A, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_FORWARD_PWM_A, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_FORWARD_PWM_A, 90);

	//FIN = 1, RIN = PWM
	mtr_set_mode(MOTOR0, MTR_MODE_FORWARD_PWM_B, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_FORWARD_PWM_B, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_FORWARD_PWM_B, 90);

	//FIN = 0, RIN = PWM
	mtr_set_mode(MOTOR0, MTR_MODE_REVERSE_PWM_A, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_REVERSE_PWM_A, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_REVERSE_PWM_A, 90);

	//FIN = PWM, RIN = 1
	mtr_set_mode(MOTOR0, MTR_MODE_REVERSE_PWM_B, 65);
	mtr_set_mode(MOTOR1, MTR_MODE_REVERSE_PWM_B, 80);
	mtr_set_mode(MOTOR2, MTR_MODE_REVERSE_PWM_B, 90);

	// Changes the period
	mtr_set_period(50, MOTOR0);
	mtr_set_period(50, MOTOR1);
	mtr_set_period(50, MOTOR2);

	//FIN = 1, RIN = 0
	mtr_set_mode(MOTOR0, MTR_MODE_FORWARD, 30);
	mtr_set_mode(MOTOR1, MTR_MODE_FORWARD, 40);
	mtr_set_mode(MOTOR2, MTR_MODE_FORWARD, 50);

	//FIN = 0, RIN = 1
	mtr_set_mode(MOTOR0, MTR_MODE_REVERSE, 30);
	mtr_set_mode(MOTOR1, MTR_MODE_REVERSE, 40);
	mtr_set_mode(MOTOR2, MTR_MODE_REVERSE, 50);

	//FIN = 1, RIN = 1
	mtr_set_mode(MOTOR0, MTR_MODE_BRAKE, 30);
	mtr_set_mode(MOTOR1, MTR_MODE_BRAKE, 40);
	mtr_set_mode(MOTOR2, MTR_MODE_BRAKE, 50);

	while(1);
/* USER CODE END */
}

/* USER CODE BEGIN (4) */
/* USER CODE END */
