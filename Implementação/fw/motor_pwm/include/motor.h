/**
 *	\file motor.h
 *	\brief Definition of the auxiliary functions to control the magnetotorquers.
 *	\author Davyd Melo
 */

#include "hal_stdtypes.h"

#ifndef CONASAT_INCLUDE_MOTOR_H_
#define CONASAT_INCLUDE_MOTOR_H_

#define MTR_MODE_STANDBY 	   0x00 /** Stand-by mode (FIN = 0 and RIN = 0). */
#define MTR_MODE_FORWARD 	   0x01 /** Foward mode. */
#define MTR_MODE_REVERSE 	   0x02 /** Reverse mode. */
#define MTR_MODE_BRAKE 		   0x03
#define MTR_MODE_FORWARD_PWM_A 0x04
#define MTR_MODE_REVERSE_PWM_A 0x05
#define MTR_MODE_FORWARD_PWM_B 0x06
#define MTR_MODE_REVERSE_PWM_B 0x07
#define MTR_MODE_FORWARD_VREF  0x08
#define MTR_MODE_REVERSE_VREF  0x09

#define MOTOR0 0x00
#define MOTOR1 0x01
#define MOTOR2 0x02

#define MTR0_FIN 0x00
#define MTR0_RIN 0x01
#define MTR1_FIN 0x02
#define MTR1_RIN 0x03
#define MTR2_FIN 0x04
#define MTR2_RIN 0x05

void mtr_init(void);


void mtr_set_mode(uint8 motor, uint8 mode, uint8 duty);


void mtr_set_period(float64 p, uint8 motor);

#endif /* CONASAT_INCLUDE_MOTOR_H_ */
