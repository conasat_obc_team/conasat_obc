/**
 *  @file bridge.c
*   @brief [OpenOBC] Bridge SPI-I2C Driver Implementation File
*/

#include "bridge.h"
#include "het.h"

//! Array with the references to the registers of the two bridges of the OBC board.
spiBASE_t* bridge_spi_lut[] = { spiREG2, spiREG3 };

//! Disables the CS on each transaction after a delay of T2CDELAY and enables the FORMAT0 of the SPI. */
spiDAT1_t bdg_dtconf = { .CS_HOLD = TRUE, .WDEL = TRUE, .DFSEL = SPI_FMT_0, .CSNR = 0U };

//! Select the order of the bits read by the SPI.
uint8 g_spiconf = BDG_SPI_CONFIG_MSB_FIRST;

/**
 * 	@brief Initialize the state of the GIO signals (WAKE_BRIDGE and RESET_BRIDGE) for
 * 	the bridge initialization process.
 *
 *  @return This function returns nothing.
 */
void bdg_init()
{
	uint16 d = 0;

	//
	// Bridge - Transponder
	//
	for (; d < 5800; d++) { asm(" nop"); }
	gioSetBit(hetPORT1, 22, 1);
	gioSetBit(gioPORTA, 1, 1);
	gioSetBit(spiPORT2, 3, 1); //CS
	for (; d < 5800; d++) { asm(" nop"); }

	//
	// Bridge - Subsystems
	//
	for (; d < 5800; d++) { asm(" nop"); }
	gioSetBit(hetPORT1, 0, 1);
	gioSetBit(hetPORT1, 2, 1);
	gioSetBit(spiPORT3, 0, 1); //CS
	for (; d < 5800; d++) { asm(" nop"); }
}

/**
 * 	@brief Writes a 'data' buffer to a slave identified by 'slave_address'.
 *
 *	@param bridge_id: The ID of the bridge:
 	 	 		  	  - BDG_ID_TRANSPONDER
 	 	 		  	  - BDG_ID_SUBSYSTEM

 *	@param slave_address - The address of the I2C slave. Normally this address is specified in
 *	the datasheet of the slave.
 *
 *	@param data - The data buffer that will be send to the slave.
 *
 *	@param size - The size of the data buffer.
 *
 *  @return This function returns nothing.
 */
void bdg_write_slave(uint8 bridge_id,
				uint8 slave_address,
				const uint16* data,
				uint8 size)
{
	uint8 idx, tries = 10;
	uint16* spi_data = (uint16*) calloc(size + 3, sizeof(uint16));
	uint16 intreg = 0;

	spi_data[0] = 0x0000;
	spi_data[1] = size;
	spi_data[2] = (slave_address << 1) & 0xFFFE; // Shift the address and clean the LSB

	for (idx = 0; idx < size; idx++)
		spi_data[idx + 3] = data[idx];

	bdg_enable_cs(bridge_id);
	spiTransmitData(bridge_spi_lut[bridge_id], &bdg_dtconf, size + 3, spi_data);
	bdg_disable_cs(bridge_id);

	free(spi_data);

	do
	{
		bdg_read_internal_reg(bridge_id, BDG_REG_ADDR_I2CSTAT, &intreg);
	}
	while(intreg != 0x00F0 && tries-- > 0);
}

/**
 * 	@brief Reads a 'data' buffer from a slave identified by 'slave_address'.
 *
 *	@param bridge_id: The ID of the bridge:
 	 	 		  	  - BDG_ID_TRANSPONDER
 	 	 		  	  - BDG_ID_SUBSYSTEM

 *	@param slave_address - The address of the I2C slave. Normally this address is specified in
 *	the datasheet of the slave.
 *
 *	@param data - The data buffer that will be write with the data from the slave.
 *
 *	@param size - The amount of bytes to be read from the slave.
 *
 *  @return This function returns SUCCESS or an error code.
   	   	   	   	   	  - SUCCESS (0x00)
					  - BDG_ERROR_INVALID_PARAM (0x01)
					  - BDG_ERROR_MEM_ALLOC (0x02)
					  - BDG_ERROR_TIMEOUT	(0x03)
 */
uint8 bdg_read_slave(uint8 bridge_id,
			   uint8 slave_address,
			   uint8 size,
			   uint16* data)
{
	uint8 tries = 5;
	uint16 spi_data[] = { 0x0001, 0x0000, 0x0000 };
	uint16 intreg = 0;

	spi_data[1] = size;
	spi_data[2] = (slave_address << 1) | 0x0001; // Shift the address and set the LSB

	bdg_enable_cs(bridge_id);
	spiTransmitData(bridge_spi_lut[bridge_id], &bdg_dtconf, 3U, spi_data);
	bdg_disable_cs(bridge_id);

	do
	{
		bdg_read_internal_reg(bridge_id, BDG_REG_ADDR_I2CSTAT, &intreg);
	}
	while(intreg != 0x00F0 && tries-- > 0);

	if (tries > 0)
	{
		return bdg_read_buffer(bridge_id, data, size);
	}
	else
	{
		return BDG_ERROR_TIMEOUT;
	}
}

/**
 * 	@brief Reads the buffer from the bridge to retrieve the data sent by the slaves.
 *
 *	@param bridge_id: The ID of the bridge:
 	 	 		  	  - BDG_ID_TRANSPONDER
 	 	 		  	  - BDG_ID_SUBSYSTEM

 *	@param data - A pointer to a memory piece that will store the data.
 *
 *	@param size - The size of the data buffer.
 *
 *  @return This function returns SUCCESS or an error code.
   	   	   	   	   	  - SUCCESS (0x00)
					  - BDG_ERROR_INVALID_PARAM (0x01)
					  - BDG_ERROR_MEM_ALLOC (0x02)
 */
uint8 bdg_read_buffer(uint8 bridge_id,
				uint16 *data,
				uint8 size)
{
	uint16* spi_data = NULL;
	uint16* spi_data_rx = NULL;
	uint8 i = 0;

	if (size == 0 || data == NULL)
		return BDG_ERROR_INVALID_PARAM;

	spi_data = (uint16*) calloc(size + 1, sizeof(uint16));

	if (spi_data == NULL)
	{
		return BDG_ERROR_MEM_ALLOC;
	}

	spi_data_rx = (uint16*) calloc(size + 1, sizeof(uint16));

	if (spi_data_rx == NULL)
	{
		return BDG_ERROR_MEM_ALLOC;
	}

	spi_data[0] = 0x0006;

	bdg_enable_cs(bridge_id);
	spiTransmitAndReceiveData(bridge_spi_lut[bridge_id], &bdg_dtconf, size + 1, spi_data, spi_data_rx);
	bdg_disable_cs(bridge_id);

	for (i = 0; i < size; i++)
	{
		data[i] = spi_data_rx[size - i];
	}

	free(spi_data);
	free(spi_data_rx);

	return SUCCESS;
}

/**
 * 	@brief Enables the Chip Select (CS) of the bridge identified by 'bridge_id'.
 *
 * 	@param bridge_id: The ID of the bridge:
 	 	 		  	  - BDG_ID_TRANSPONDER
 	 	 		  	  - BDG_ID_SUBSYSTEM
 *
 *  @return This function returns nothing.
 */
void bdg_enable_cs(uint8 bridge_id)
{
	uint16 delay = 0;

	if (bridge_id == BDG_ID_TRANSPONDER)
	{
		gioSetBit(spiPORT2, 3, 0);
	}
	else
	{
		gioSetBit(spiPORT3, 0, 0);
	}

	for (delay = 0; delay < 5000; delay++) { asm(" nop"); }
}

/**
 * 	@brief Disables the Chip Select (CS) of the bridge identified by 'bridge_id'.
 *
 *  @param bridge_id: The ID of the bridge:
 	 	 		  	  - BDG_ID_TRANSPONDER
 	 	 		  	  - BDG_ID_SUBSYSTEM
 *
 *  @return This function returns nothing.
 */
void bdg_disable_cs(uint8 bridge_id)
{
	uint16 delay = 0;
	for (delay = 0; delay < 5000; delay++) { asm(" nop"); }

	if (bridge_id == BDG_ID_TRANSPONDER)
	{
		gioSetBit(spiPORT2, 3, 1);
	}
	else
	{
		gioSetBit(spiPORT3, 0, 1);
	}
}

/**
 * 	@brief Writes an internal register of the SPI-I2C bridge.
 *
 *  @param bridge_id: The ID of the bridge:
 	 	 		  	  - BDG_ID_TRANSPONDER
 	 	 		  	  - BDG_ID_SUBSYSTEM

 	@param reg_address: The address of the internal register. Check the bridge datasheet.
 						- BDG_REG_ADDR_IOCONFIG (0x00)
						- BDG_REG_ADDR_IOSTATE  (0x01)
						- BDG_REG_ADDR_I2CCLOCK (0x02)
						- BDG_REG_ADDR_I2CTO    (0x03)
						- BDG_REG_ADDR_I2CSTAT  (0x04)
						- BDG_REG_ADDR_I2CADR   (0x05)

 	@param reg_data: The data to be writen in the register.
 *
 *  @return This function returns SUCCESS or BDG_ERROR_INVALID_PARAM when the reg_address is invalid.
 */
uint8 bdg_write_internal_reg(uint8 bridge_id,
					   uint8 reg_address,
					   uint8 reg_data)
{
	uint16 spi_data[] = { 0x20, 0x00, 0x00 };

	// Cases when the write is not possible
	if (reg_address == BDG_REG_ADDR_I2CSTAT || reg_address > BDG_REG_ADDR_I2CADR)
	{
		return BDG_ERROR_INVALID_PARAM;
	}

	spi_data[1] = reg_address;
	spi_data[2] = reg_data;

	bdg_enable_cs(bridge_id);
	spiTransmitData(bridge_spi_lut[bridge_id], &bdg_dtconf, 3U, spi_data);
	bdg_disable_cs(bridge_id);

	return SUCCESS;
}

/**
 * 	@brief Reads an internal register of the SPI-I2C bridge.
 *
 *  @param bridge_id: The ID of the bridge:
 	 	 		  	  - BDG_ID_TRANSPONDER
 	 	 		  	  - BDG_ID_SUBSYSTEM

 	@param reg_address: The address of the internal register. Check the bridge datasheet.
 						- BDG_REG_ADDR_IOCONFIG (0x00)
						- BDG_REG_ADDR_IOSTATE  (0x01)
						- BDG_REG_ADDR_I2CCLOCK (0x02)
						- BDG_REG_ADDR_I2CTO    (0x03)
						- BDG_REG_ADDR_I2CSTAT  (0x04)
						- BDG_REG_ADDR_I2CADR   (0x05)

 	@param reg_value: A pointer to the variable to be written with the register value.
 *
 *  @return This function returns SUCCESS or BDG_ERROR_INVALID_PARAM when the 'reg_address' is invalid.
 */
uint8 bdg_read_internal_reg(uint8 bridge_id,
					  uint8 reg_address,
					  uint16* reg_value)
{
	uint16 spi_data[] = { 0x0021, 0x0000, 0x0000 };
	uint16 spi_response[] = { 0x0000, 0x0000, 0x0000 };

	if (reg_address > BDG_REG_ADDR_I2CADR)
	{
		return BDG_ERROR_INVALID_PARAM;
	}

	spi_data[1] = reg_address;
	spi_data[2] = 0x0000; // Dummy data.

	bdg_enable_cs(bridge_id);
	spiTransmitAndReceiveData(bridge_spi_lut[bridge_id], &bdg_dtconf, 3U, spi_data, spi_response);
	bdg_disable_cs(bridge_id);

	if (g_spiconf == BDG_SPI_CONFIG_MSB_FIRST)
		*reg_value = (spi_response[2] & 0x00FF);
	else
		*reg_value = (spi_response[0] & 0x00FF);

	return SUCCESS;
}
