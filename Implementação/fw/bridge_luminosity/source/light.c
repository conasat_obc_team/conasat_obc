#include "light.h"
#include "bridge.h"
#include <math.h>

//uint8_t bridge_id = BDG_ID_TRANSPONDER;
uint8_t bridge_id = BDG_ID_SUBSYSTEM;

void ls_write_byte(unsigned char address, unsigned char value)
{
	uint16 txdata[] = { (address & 0x0F) | TSL2561_CMD, value };

	bdg_write_slave(bridge_id, 0x39, txdata, 2U);
}

void ls_read_byte(uint8_t address, uint8_t* value)
{
	uint16 txdata[] = { (address & 0x0F) | TSL2561_CMD };
	uint16 rxdata[] = { 0x0000 };

	bdg_write_slave(bridge_id, 0x39, txdata, 1U);
	bdg_read_slave(bridge_id, 0x39, 1U, rxdata);

	*value = (uint8_t) (rxdata[0] & 0x00FF);
}

uint16_t ls_read_adc0()
{
	uint8_t vl, vh = 0;

	ls_read_byte(TSL2561_REG_DATA0L, &vl);
	ls_read_byte(TSL2561_REG_DATA0H, &vh);

	return (uint16_t)(vh * 256 + vl);
}

uint16_t ls_read_adc1()
{
	uint8_t vl, vh = 0;

	ls_read_byte(TSL2561_REG_DATA1L, &vl);
	ls_read_byte(TSL2561_REG_DATA1H, &vh);

	return (uint16_t)(vh * 256 + vl);
}

void ls_power_up()
{
	ls_write_byte(TSL2561_REG_CONTROL, 0x03);
}

double ls_convert_to_lux(unsigned int ch0, unsigned int ch1)
	// Convert raw data to lux
	// gain: 0 (1X) or 1 (16X), see setTiming()
	// ms: integration time in ms, from setTiming() or from manual integration
	// CH0, CH1: results from getData()
	// lux will be set to resulting lux calculation
	// returns true (1) if calculation was successful
	// RETURNS false (0) AND lux = 0.0 IF EITHER SENSOR WAS SATURATED (0XFFFF)
{
	double ratio, d0, d1;
	unsigned int ms = 402;
	unsigned char gain = 0;

	// Determine if either sensor saturated (0xFFFF)
	// If so, abandon ship (calculation will not be accurate)
	if ((ch0 == 0xFFFF) || (ch1 == 0xFFFF))
	{
		return 0.0;
	}

	// Convert from unsigned integer to floating point
	d0 = ch0; d1 = ch1;

	// We will need the ratio for subsequent calculations
	ratio = d1 / d0;

	// Normalize for integration time
	d0 *= (402.0/ms);
	d1 *= (402.0/ms);

	// Normalize for gain
	if (!gain)
	{
		d0 *= 16;
		d1 *= 16;
	}

	// Determine lux per datasheet equations:

	if (ratio < 0.5)
	{
		return 0.0304 * d0 - 0.062 * d0 * pow(ratio,1.4);
	}

	if (ratio < 0.61)
	{
		return 0.0224 * d0 - 0.031 * d1;
	}

	if (ratio < 0.80)
	{
		return 0.0128 * d0 - 0.0153 * d1;
	}

	if (ratio < 1.30)
	{
		return 0.00146 * d0 - 0.00112 * d1;
	}

	return 0;
}
