#ifndef INCLUDE_LIGHT_H_
#define INCLUDE_LIGHT_H_

#define TSL2561_CMD         0x80
#define	TSL2561_REG_ID      0x0A
#define	TSL2561_REG_CONTROL 0x00
#define TSL2561_REG_DATA0L  0x0C
#define TSL2561_REG_DATA0H  0x0D
#define TSL2561_REG_DATA1L  0x0E
#define TSL2561_REG_DATA1H  0x0F

#include "light.h"
#include "bridge.h"

void ls_write_byte(unsigned char address, unsigned char value);
void ls_read_byte(uint8_t address, uint8_t* value);
uint16_t ls_read_adc0();
uint16_t ls_read_adc1();
void ls_power_up();
double ls_convert_to_lux(unsigned int ch0, unsigned int ch1);

#endif
