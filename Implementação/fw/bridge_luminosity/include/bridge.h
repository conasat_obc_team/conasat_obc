/*
 * bridge.h - A short description
 *
 * Created on: 23/11/2015
 * Author: Davyd Melo (davydmelo@gmail.com)
 * Reviewer: J�lio Cesar (juliocesaraf@gmail.com)
 * Review Date:
 */

#include "error.h"
#include "spi.h"
#include "gio.h"
#include "hal_stdtypes.h"
#include "stdlib.h"

#ifndef CONASAT_INCLUDE_BRIDGE_H_
#define CONASAT_INCLUDE_BRIDGE_H_

extern uint8 bytes_to_read;

#define BDG_ID_TRANSPONDER 0
#define BDG_ID_SUBSYSTEM   1

//
// SPI configuration values.
//
#define BDG_SPI_CONFIG_LSB_FIRST 0x81
#define BDG_SPI_CONFIG_MSB_FIRST 0x42 //Default

//
// Internal register addresses.
//
#define BDG_REG_ADDR_IOCONFIG 0x00
#define BDG_REG_ADDR_IOSTATE  0x01
#define BDG_REG_ADDR_I2CCLOCK 0x02
#define BDG_REG_ADDR_I2CTO    0x03
#define BDG_REG_ADDR_I2CSTAT  0x04
#define BDG_REG_ADDR_I2CADR   0x05

//
// Values for the status register in case of interrupts.
//
#define BDG_STATUS_TX_SUCESSFUL       0xF0
#define BDG_STATUS_SLV_ADDR_NOT_ACK   0xF1
#define BDG_STATUS_SLV_BYTE_NOT_ACK   0xF2
#define BDG_STATUS_I2C_BUSY           0xF3
#define BDG_STATUS_I2C_TIMOUT         0xF8
#define BDG_STATUS_I2C_INVALID_DCOUNT 0xF9

//
// Functions prototypes
//
void bdg_init(void);
void bdg_write_slave(uint8 bridge_id, uint8 slave_address, const uint16* data, uint8 size);
uint8 bdg_read_slave(uint8 bridge_id, uint8 slave_address, uint8 size, uint16* data);
uint8 bdg_read_buffer(uint8 bridge_id, uint16 *data, uint8 size);
void bdg_enable_cs(uint8 bridge_id);
void bdg_disable_cs(uint8 bridge_id);
uint8 bdg_write_internal_reg(uint8 bridge_id, uint8 reg_address, uint8 reg_data);
uint8 bdg_read_internal_reg(uint8 bridge_id, uint8 reg_address, uint16* reg_value);

#endif /* CONASAT_INCLUDE_BRIDGE_H_ */
