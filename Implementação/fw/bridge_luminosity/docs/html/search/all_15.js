var searchData=
[
  ['valid0',['VALID0',['../structdcc_base.html#ac29671000a12bbde883436930518e29c',1,'dccBase']]],
  ['valid0seed',['VALID0SEED',['../structdcc_base.html#a6201ae1d64a32547c323cc8f4b39c889',1,'dccBase']]],
  ['value',['value',['../structadc_data.html#a4dd7e7f197e82dc6db279eed0a5dc820',1,'adcData']]],
  ['vclk1_5ffreq',['VCLK1_FREQ',['../system_8h.html#a92d1bf5047c4525efc4699e09db47aa9',1,'system.h']]],
  ['vclk2_5ffreq',['VCLK2_FREQ',['../system_8h.html#aad0c9c7796f8cc004ab3fc96b0bb3ab5',1,'system.h']]],
  ['vclk3_5ffreq',['VCLK3_FREQ',['../system_8h.html#a38973c3e9cc044e6246e5b6b6ff4462c',1,'system.h']]],
  ['vclk4_5ffreq',['VCLK4_FREQ',['../system_8h.html#a65a076021e41a87bb965b161203b6853',1,'system.h']]],
  ['vim',['VIM',['../group___v_i_m.html',1,'']]],
  ['vim_5fconfig_5freg',['vim_config_reg',['../structvim__config__reg.html',1,'']]],
  ['vimbase',['vimBase',['../structvim_base.html',1,'']]],
  ['vimbase_5ft',['vimBASE_t',['../reg__vim_8h.html#a5af5a10f0368329b249ae77e6a7d4938',1,'reg_vim.h']]]
];
