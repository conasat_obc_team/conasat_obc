var searchData=
[
  ['bdg_5fdisable_5fcs',['bdg_disable_cs',['../bridge_8c.html#a2a61c4d568d98f16dc79b3af82014cb5',1,'bridge.c']]],
  ['bdg_5fdtconf',['bdg_dtconf',['../bridge_8c.html#a55a48ff6be5a37a67272dff7806d6d16',1,'bridge.c']]],
  ['bdg_5fenable_5fcs',['bdg_enable_cs',['../bridge_8c.html#a246f31cad0104ff6240b7b5032238349',1,'bridge.c']]],
  ['bdg_5finit',['bdg_init',['../bridge_8c.html#a57495cb880ac2cdc25026449836abdd4',1,'bridge.c']]],
  ['bdg_5fread_5fbuffer',['bdg_read_buffer',['../bridge_8c.html#a64e1f55e7ad46436343d551cb578ae97',1,'bridge.c']]],
  ['bdg_5fread_5finternal_5freg',['bdg_read_internal_reg',['../bridge_8c.html#a9ccdac9c98708f03ef2da03b9f5dfb97',1,'bridge.c']]],
  ['bdg_5fread_5fslave',['bdg_read_slave',['../bridge_8c.html#a96fce5c93c14facdc02060c3ff63813e',1,'bridge.c']]],
  ['bdg_5fwrite_5finternal_5freg',['bdg_write_internal_reg',['../bridge_8c.html#acc31c5886cf81ce5fcfff9909e43b296',1,'bridge.c']]],
  ['bdg_5fwrite_5fslave',['bdg_write_slave',['../bridge_8c.html#a14b7f30b87f79532cc0d7bf2477c02b1',1,'bridge.c']]],
  ['bridge_2ec',['bridge.c',['../bridge_8c.html',1,'']]],
  ['bridge_5fspi_5flut',['bridge_spi_lut',['../bridge_8c.html#a9e189f9c5f4ca168875ece67156f0ec7',1,'bridge.c']]]
];
