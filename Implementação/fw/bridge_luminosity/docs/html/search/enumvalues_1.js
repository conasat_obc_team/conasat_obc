var searchData=
[
  ['dcc1_5fcnt0_5fhf_5flpo',['DCC1_CNT0_HF_LPO',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489a38a952b6f05f0ca43033aa7110a2180d',1,'dcc.h']]],
  ['dcc1_5fcnt0_5foscin',['DCC1_CNT0_OSCIN',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489ac5bf7c15709072e6f4b6592fafc3c6a4',1,'dcc.h']]],
  ['dcc1_5fcnt0_5ftck',['DCC1_CNT0_TCK',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489a550d294ba66dd6460ee109bde7193340',1,'dcc.h']]],
  ['dcc1_5fcnt1_5fextclkin1',['DCC1_CNT1_EXTCLKIN1',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489ab0c4ce54442bdc6a37f639dfa92d070d',1,'dcc.h']]],
  ['dcc1_5fcnt1_5fhf_5flpo',['DCC1_CNT1_HF_LPO',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489a07fd22a6a3b3b2d023fb3a9e781f2f1f',1,'dcc.h']]],
  ['dcc1_5fcnt1_5flf_5flpo',['DCC1_CNT1_LF_LPO',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489a544d3c4262140a91ad6d45fba9faf70f',1,'dcc.h']]],
  ['dcc1_5fcnt1_5fn2het1_5f31',['DCC1_CNT1_N2HET1_31',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489ae50fe282fa0148428d3ac92a76fa3a2f',1,'dcc.h']]],
  ['dcc1_5fcnt1_5fpll1',['DCC1_CNT1_PLL1',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489abce2b2a0462a9e5d8a8d36c856d2cfe8',1,'dcc.h']]],
  ['dcc1_5fcnt1_5fvclk',['DCC1_CNT1_VCLK',['../dcc_8h.html#a85353d58e849e1abaa682c468b2cb489ae4917223e93a59be956f7fde9a3dc8db',1,'dcc.h']]]
];
